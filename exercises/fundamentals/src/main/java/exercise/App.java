package exercise;

class App {
    public static void numbers() {
        // BEGIN
        System.out.println(8 / 2 + 100 % 3);
        // END
    }

    public static void strings() {
        String language = "Java";
        String sentance = " works on JVM";
        // BEGIN
        System.out.println(language + sentance);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println(soldiersCount + " " + name);
        // END
    }
}
